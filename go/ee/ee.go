// Copyright Luke Johnson
// https://creativecommons.org/licenses/by/4.0/
package main

import (
    "fmt"
    "math"
    "os"
    "strconv"
    "strings"
)

const (
    E = 'E'
    b = ' '
)
var E1 = [][]byte{
    []byte{E, E, E, E, E},
    []byte{E, b, b, b, b},
    []byte{E, E, E, E, b},
    []byte{E, b, b, b, b},
    []byte{E, E, E, E, E},
}

func min(a, b int) int {
    if a < b {
        return a
    }
    return b
}


func charAt(x, y, order int) byte {
    switch {
    case order < 1:
        return E
    case order == 1:
        return E1[y][x]
    }
    if charAt(x/5, y/5, order - 1) == E {
        return E1[y%5][x%5]
    }
    return b
}

func produce(order int) {
    if order == 0 {
        fmt.Println(string(E))
        return
    }
    side := int(math.Pow(5, float64(order)))
    for y := 0; y < side; y++ {
        line := make([]byte, side)
        for x := 0; x < side; x++ {
            line[x] = charAt(x, y, order)
        }
        truncated := strings.TrimSpace(string(line))
        inlen := len(truncated)
        outlen := inlen + inlen/5
        output := make([]byte, outlen)
        for i, o := 0, 0; o < outlen + 1; i, o = i + 5, o + 6 {
            i_end := min(i+5, inlen)
            o_end := min(o+5, outlen)
            copy(output[o:o_end], truncated[i:i_end])
            if i_end < inlen {
                output[o_end] = b
            }
        }
        fmt.Println(string(output))
    }
}

func main() {
    if order, err := strconv.Atoi(os.Args[1]); err != nil {
        fmt.Println(err)
        os.Exit(1)
    } else {
        produce(order)
    }
}
