Generate ASCII art recursive Es

Order *0* is:
```
E
```

Order *1* is:
```
EEEEE
E
EEEE
E
EEEEE
```

Order *2* is:
```
EEEEE EEEEE EEEEE EEEEE EEEEE
E     E     E     E     E
EEEE  EEEE  EEEE  EEEE  EEEE
E     E     E     E     E
EEEEE EEEEE EEEEE EEEEE EEEEE
EEEEE
E
EEEE
E
EEEEE
EEEEE EEEEE EEEEE EEEEE
E     E     E     E
EEEE  EEEE  EEEE  EEEE
E     E     E     E
EEEEE EEEEE EEEEE EEEEE
EEEEE
E
EEEE
E
EEEEE
EEEEE EEEEE EEEEE EEEEE EEEEE
E     E     E     E     E
EEEE  EEEE  EEEE  EEEE  EEEE
E     E     E     E     E
EEEEE EEEEE EEEEE EEEEE EEEEE
```

...and so forth.

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.
