#!/usr/bin/python3
# Copyright Luke Johnson
# https://creativecommons.org/licenses/by/4.0/

import sys

E = 'E'
b = ' '
E1 = (
    (E, E, E, E, E),
    (E, b, b, b, b),
    (E, E, E, E, b),
    (E, b, b, b, b),
    (E, E, E, E, E),
)   

def charAt(x, y, order):
    if order == 1:
        return E1[y][x]
    c = charAt(int(x/5), int(y/5), order - 1)
    if c == E:
        return E1[y%5][x%5]
    return b

def produce(order):
    if order == 0:
        print('E')
        return
    for y in range(0, 5**order):
        line = []
        for x in range(0, 5**order):
            line.append(charAt(x, y, order))
        print(' '.join(
            [''.join(line[x:y]) 
             for x, y in zip(range(0, len(line), 5), 
                             range(5, len(line)+1, 5))]))
        
produce(int(sys.argv[1]))
